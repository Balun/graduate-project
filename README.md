# Segmentacija slike u maritimnom okruženju za autonomno plovilo primjenom dubokog učenja
Matej Balun, Sveučilište u Zagrebu, Fakultet Elektrotehnike i Računarstva

## 1. Uvod
U današnje doba umjetna inteligencija, specifično njena najpoznatija grana
zvana strojno učenje veoma je raširen ne samo koncept nego i praktična primjena
inteligentnih pristupa i algoritama rješavanja mnogih problema koji nisu bili
rješivi tradicionalnim metodama računarske znanosti. 

Jedan od najzanimljivijih tehnoloških problema današnjice je autonomna vožnja,
tj nastojanje da se u sustave za transport ljudi i dobara uvede autonomija
koja bi zamijenila mnoge manualne i rezidualne radnje u obavljanju društvenih
djelatnosti. U ovome trenutku može vidjeti jako kvalitetne primjere postignute
autonomije u zraku pomoću bespilotnih letjelica (eng. *dronova*), kao i u 
cestovnom prometu, što možemo vidjeti na najnovijim autonomnim razvojnim 
projektima tvrtki poput *Tesla* i drugih. Ono što je nekako zanemareno u ovom
trenutku je postizanje autonomije u maritimnom okruženju, s naglaskom na morima
i jezerima, te je tema ovog projekta jedan od ključnih dijelova autonomnog
sustava: vizualna percepcija tj. računalni vid.

U ovome projektu istražen je koncept vizualne segmentacije objekata na slikama
dobivenih kamerom koja se nalazi instalirana na autonomnom morskom plovilu.
Primijenjene su metode dubokog učenja pomoću generativnih dubokih modela, te
su korištena poznata programska rješenja i strukture radi dobivanja stabilnije
i strukturiranije implementacije.

## 2. Segmentacija slike
U polju računalnog vida, segmentacija slike je proces particioniranja digitalne
slike u više segmenata, gdje svaki segment predstavlja skupinu piksela koji
zajedno imaju neko semantičko značenje. Segmentacija se najčešće koristi kako
bi se odredile granice između određenih regija/objekata na slici. U suštini,
segmentacija je proces davanja oznake (labele) svakom pikselu slike tako da
isto označeni pikseli dijele određene karakteristike.

Segmentacija slike je jedna od ključnih prepreka u postizanju autonomije u 
prometu. Određivanje granice između interesnih područja u slici govori nam 
koji su dijelovi slike pozitivni za praćenje u našem autonomnom sustavu, a koji
su negativni te ih trebamo izbjegavati. Tako recimo pri postizanju autonomije
u cestovnom prometu možemo pokušati odrediti gdje se na slici nalazi kolnik, 
gdje pločnik a di zgrade itd.

Primjena segmentacije u ovome radu biti ć€ u maritimnom okruženju kao što je 
i rečeno, a njome će se nastojati riješiti ključan problem lociranja mora, 
kopna, neba i prepreka. S oborom da je segmentacija za spomenutu primjenu
podosta kompleksan problem, prvo je valjalo krenuti s jednostavnijim
 segmentacijskim problemima, kako bi se isprobale tehnike i definirao model
 dubokog učenja koji bi se uz određene preinake mogao koristiti za spomenutu
 svrhu.
 
![Primjer segmentacije u cestovnom prometu](doc/img/2.png)
 
## 3. Jednostavna segmentacija na poznatom skupu podataka

### 3.1 Općenito
Kao što je rečeno, cilj segmentacije je označiti svaki piksel te ga tako
klasificirati određenu regiju slike. Dakle ulaz u sustav je skup slika s
njihov maskama - poljima koja su istih dimenzija kao i ulazne slike te
sadrže informaciju za svaki piksel kojoj klasi pripada. Već ovdje se može
vidjeti kako se radi o problemu mnogo složenijem od standardne klasifikacije
u računalnom vidu, gdje je potrebno točno klasificirati sliku kao jedinku.
U tu svrhu ćemo koristiti generativne duboke modele koje poopćeno zovemo
autoenkoderi, no više o samoj implementaciji modela kasnije.

### 3.2 Oxford-IIIT Pets skup podataka
Oxford-IIIT Pets skup podataka sadrži 37 različitih kategorija fotografija
kućnih ljubimaca, sa ugrubo 200 slika za svaku kategoriju. Slike su međusobno
raznolike ne samo po kategorijama, nego i po nagibu objekata, osvijetljenu, 
šumu itd. Sve slike su anotirane svojim maskama (ozakama piksela), i to s 
moguće tri kategorije za segmentaciju:
 - **Klasa 0**: Piksel koji pripada ljubimcu
 - **Klasa 1**: Piksel koji pripada Graničnom području ljubimca
 - **Klasa 2**: Piksel koji ne pripada ljubimcu
 
![Primjer Oxford skupa podataka](doc/img/1.png)

### 3.3 Programski alati za ostvarenje jednostavne segmentacije
TensorFlow je danas već zadnjih nekoliko godina najpopularniji programski alat
za obradu podataka i diferencijalno programiranje, a u računarskoj znanosti ga
intenzivno koristimo za izvršavanje matematičkih operacija i modela nužnih za
implementiranje algoritama dubokog učenja.

Ovdje je TensorFlow korišten ujedno i za pripremu podataka za učenje (obradu)
slike, kao i za implementiranje modela dubokog učenja i samo njegovo učenje.
Pomoću njega smo dohvatili skup podataka, normalizirali slike te obavili
transformacije na njima za dobivanje raznolikosti podataka, te je implementiran
prikaz istih. 

### 3.4 Rezultati jednostavne segmentacije
Duboki autoenkoderski model korišten za ovu primjenu opisan je i korišten
kasnije u složenijoj segmentaciji. Učenje je provedeno optimizacijskim
Adamovim algoritmom, funkcija gubitka je rijetka kategorijska unakrsna
entropija, te je kao referentna metrika uzeta preciznost. Model je treniran
s 3200 označenih primjera iz skupa za učenje, te validiran višestrukim
preklapanjem primjerima iz skupa za testiranje, kojih je xxx. Učenje je
provedeno kroz 20 poha, validacija je obavljena sa k-foldom, gdje je k=5.
Rezultati su sljedeći su po redu s nenaučenim modelom, naučenim modelom i 
grafom gubitka učenja i validacije.

![Nenaučen model](doc/img/3.png)

![Naučen model](doc/img/4.png)

![Graf učenja i validacije](doc/img/5.png)

Kao što možemo vidjeti, Nenaučen model logično ne pokazuje dobre rezultate, 
međutim nakon 20 epoha učenja možemo vidjeti poprilično zadovoljavajuće
rezultate kako na skupu za učenje tako i pri validaciji. Graf pokazuje
kako je gubitak naravno manji pri učenju, i to je dobra i standardna pojava.
U nastavku možemo vidjeti još nekoliko primjera uspješne segmentacije na ovom
skupu:

![Različiti primjeri segmentacije](doc/img/6.png)

## 4. Duboki autoenkoderski model korišten u implementaciji

### 4.1 Općenito o autoenkoderima
Autoenkoder je tip umjetne neuronske mreže koji se koristi za kodiranje 
podataka u efikasnom tonu. Cilj autoenkodera je naučiti reprezentaciju
(enkoder) podatka tipično s reduciranom dimenzionalnosti, trenirajući
neuronsku mrežu da ignorira šum i ostale nebitne informacije. Nakon redukcije
dolazi rekonstrukcijski dio autoenkodera (dekoder) koji se također uči, 
koji tipično nastoji proizvesti izlaz istih dimenzija kao i ulaz, s opcijom
različitih tranformacija u onosu na ulaz itd. 

![Tipična autoenkoderska arhitektura](doc/img/7.png)

### 4.2  UNet - Autoenkoderski konvolucijski model
U ovome radu idejni osnovni model autoenkoder je *UNet*. UNet je duboka
konvolucijska autoenkoderska arhitektura koja je pokazala jako dobre primjene
u medicini, specifično za segmentaciju medicinskih slika poput magnetske
rezonance i sličnih, a i pokazuje se da kao koncept radi jako dobro i u
segmentaciji slike u prometu. Enkoderski dio je klasična konvolucijska mreža,
sastavljena od unakrsnih Konvolucija i slojeva sažimanja. Dekoderski dio je
također slijedno sastavljen od konvolucijskih slojeva, samo što umjesto 
klasičnog sažimanja struktura proširuje tok podataka, sve do izlaza iste
dimenzionalnosti kao i ulaza. 

Konkretno, u ovom slučaju enkoderski dio je MobileNetV2 arhitektura, dok je 
dekoderski dio Pix2pix. Oba modela su predefinirana i otvorena za promjene
u arhitekturi kako bi se prilagodile podacima. Valja napomenuti da se radi o
modificiranom UNet modeluStruktura kompletnog autoekodera je vidljiva ovdje:

![Arhitektura modela](doc/img/8.png)

S obzirom da se radi o poprilično složenoj arhitekturi, definitivno se
preporuča izvršavanje učenja na grafičkim procesorima. Ova arhitektura
je proizvela odlične rezultate za skup podataka s ljubimcima, a rezultati
za maritimne podatke biti će komentirani u nastavku. Nužno je napomenuti
da nije utvrđeno da je ovo nužno najbolja moguća arhitektura za ov primjenu,
te se gotovo sigurno u nastavku istraživanja isprobati razne arhitekture, 
u svrhu pronalaska optimalnog modela za maritimnu vizualnu segmentaciju.

## 5. Programsko rješenje za segmentaciju slike u maritimnom okruženju
### 5.1 Skup podataka MaSTr1325
MaSTr1325 je novi široki skup podataka namijenjen za maritimnu semantičku 
segmentaciju. Primarna namjena skupa podataka je za razvijanje sustava za
detekciju prepreka za mala bespilotna plovila (engl. *Unmanned surface
vehicle, USV*). Sastoji se od 1325 raznolikih slika prikupljenih u periodu 
od dvije godine s pravim bespilotnim plovilom, pokrivajući tako realistične
uvjete za spomenutu zadaću. Sve slike su označene po pikselu, te tako tvore
pogodan segmentacijski materijal, a moguće oznake piksela su:
 - **0** - Prepreke i okolina
 - **1** - Voda (more)
 - **2** - Nebo
 - **4** - Regija za ignorirati/nepoznata kategorija
 
![Primjer ulaza](doc/img/9.jpg)

![Primjer anotacije](doc/img/10.png)

### 5.2 Priprema skupa za učenje
Tvorci skupa podataka (navedeni u literaturi) predlažu sljedeće transformacije
nad podacima, kako bi se skup regularizirao te postigla što bolja
generalizacija segmentacije za neviđene primjere:
 - Vertikalno zrcaljenje
 - Centralne rotacije za +/-[5, 15] stupnjeva
 - Elastična deformacija komponenti vode
 - Transfer boja

Spomenute transformacije definitivno treba implementirati kako bi se postigli 
što bolji rezultati, što će biti prokomentirano u dijelu o fazi učenja. 

### 5.3 Prilagodba modela
Što se tiče arhitekture modela, korištena je spomenuta arhitektura iz prošlog
poglavlja, kao i za skup podataka kućnih ljubimaca. Jedina razlika je u tome
što su ulazi i izlazi prilagođeni dimenzijama slike iz maritimnog skupa 
podataka (512, 384). Vrlo vjerojatno je potrebno napraviti dodatne modifikacije
u strukturi modela, a možda i čak izmijeniti i kompletan model, pošto iako se
radi isto o problemu segmentacije, podaci su većih dimenzija, složeniji te
u prosjeku sadrže više informacije nego podaci iz skupa ljubimaca. U daljnjem
razvoju sgurno će se eksperimentirati s arhitekturom i tipom samog modela.

### 5.4 Proces učenja, rezultati i zaključci
Učenje je provedeno višedretveno na procesoru, ali i na grafičkom procesoru,
pa ćemo ih razmotriti zasebno:
#### 5.4.1 Učenje na CPU
Učenje na CPU je zbog složenosti arhitekture mreže bilo poprilično sporo,
ali je nakon određenog vremena dalo jako dobre rezultate na skupu za 
treniranje. Konkretno, radi se o 95% preciznosti na skupu za učenje, dok je
točnost testiranja razmjerno loša (oko 57%), što možemo povezati s lošom
generalizacijom modela iz prije spomenutih razloga: Skup podataka, iako
raznolik, potrebno je regularizirati sa spomenuta 4 tipa regularizacije.
Dodatno, valja eksperimentirati i s regularizacijom samog modela, te vjerujem
da će uz ove preinake model postići vrlo dobre rezultate na ovom skupu.
#### 5.4.2 Učenje na GPU
Učenje na GPU je za nekoliko razina brže nego na CPU, daje ekvivalentne
rezultate na skupu za učenje, međutim prilikom validacije na kraju epohe
TensorFlow alocira jako veliku količinu memorije iz nepoznatog razloga,
a pošto se radi o relativni slabijem grafičkom procesoru (Nvidia GeForce 940M,
2 GB interne memorije) dolazi do iznimke za prekoračenje memorije (engl. *Out
of Memory, OOM*) te nije moguće završiti kompletan proces učenja, validacije
i testiranja. Definitivno će se analizirati o čemu se radi i upogoniti učenje
na grafičkom procesoru, očekujući veoma dobre rezultate.

## 6. Upute za korištenje
Programsko rješenje trenutačno podržava samo pokretanje iz naredbenog retka,
te bi u budućnosti moglo podržavati i grafičko korisničko sučelje. Inačica
programskog jezika Python je 3.7.5, dok je verzija programske biblioteke
TensorFlow 2.0.0.

Osnovna uporaba programskog rješenja:
```
python run.py
```
ili
```
./run.py
```
Gore navedena naredba pokreće program s pretpostavljenim direktorijem skupa
podataka za učenje: ./res/data/MaSTr1325

Ako se skup za učenje nalazi na nekoj drugoj lokaciji, programsko rješenje
može primiti kao ulaz direktorije do slika i njihovih anotacija (maski):
```
python run.py -i path/to/images -a path/to/annotations
```
ili
```
python run.py --images path/to/images --annotations path/to/annotations
```

Sve dodatne informacije mogu se pročitati pomoću:
```
python run.py --help
```
ili
```
python run.py -h
```

## 7. Zaključak
Segmentacija slike u kontekstu računalnog vida ključan je aspekt postizanja
kvalitetne i dovoljno sigurne autonomije prometnih vozila za 
regularnu upotrebu. Segmentacijom možemo locirati mjesta od interesa na 
slici te tako saznati veoma korisne informacije za donošenje zaključaka
u upravljanju, međutim i dalje preostaje riješiti par ključnih problema
računalnog vida: točna lokacija određenih regija u odnosu na proporcije plovila
i okruženja te ono najbitnije, udaljenost određenih objekata od plovila. 

Što se udaljenosti tiče, skup podataka s kojim je rađeno ima svoje proširenje
i novu verziju, koja nudi stereo slike maritimnog područja koje nas zanima.
Tako se već ulazi u područje stereo-vida, a tako u suštini i 
funkcionira određivanje udaljenosti i proporcija i u ljudskom vidu. 
Nadalje treba istražit algoritme, pristupe i modele koji su pogodni za takav
jedan stereo skup podataka, te pokušati rezultate povezati s ostatkom 
sustava a autonomnu plovidbu te ocijeniti kvalitetu autonomnog upravljanja
na temelju informacija dobivenih računalnim vidom pomoću segmentacije i 
dubokog učenja. 

## 8. Literatura
 - O. Ronneberger, P. Fischer, T. Brox: U-Net: Convolutional 
 Networks for Biomedical Image Segmentation 
 [https://arxiv.org/pdf/1505.04597.pdf](https://arxiv.org/pdf/1505.04597.pdf)
 
 - Tensorflow tutorial: Image segmentation 
 https://www.tensorflow.org/tutorials/images/segmentation
 
 - M. Sandler, A. Howard, M. Zhu, A. Zhmoginov, L.C. Chen:
 MobileNetV2: Inverted Residuals and Linear Bottlenecks
 [https://arxiv.org/pdf/1801.04381.pdf](https://arxiv.org/pdf/1801.04381.pdf)
 
 - Bovcon, Borja, J. Muhovič, J. Perš, M. Kristan: 
 The MaSTr1325 dataset for training deep USV obstacle detection models
 [http://box.vicos.si/borja/viamaro/index.html](http://box.vicos.si/borja/viamaro/index.html)

Pratite nas
Facebook stranica ispravi.meTwitter stranica ispravi.me
© 1991. - 2020. Fakultet elektrotehnike i računarstva, Sveučilište u Zagrebu. Sva prava pridržana.

