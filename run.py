"""
"""
import pathlib
import os

import src.datasets as data_utils
import src.datasets.marine as datasets
import src.img as img_utils
import src.models.segmentation as models

import tensorflow as tf
import numpy as np

APP_ROOT = pathlib.Path(__file__).absolute()
MODEL_DIR = (os.path.abspath('res/models/saved'))

if __name__ == '__main__':
    dataset = datasets.MaSTR1325()

    X_train, X_test, Y_train, Y_test = data_utils.train_test_split(
        dataset.get_images(), dataset.get_annotations())

    TRAIN_LENGTH = len(X_train)
    BATCH_SIZE = 1
    BUFFER_SIZE = 1000
    STEPS_PER_EPOCH = TRAIN_LENGTH // BATCH_SIZE

    EPOCHS = 1
    VAL_SUBSPLITS = 5
    VALIDATION_STEPS = len(X_test) // BATCH_SIZE

    X_train = img_utils.normalize(X_train)
    X_test = img_utils.normalize(X_test)

    X_train[:] = X_train[tf.newaxis, ...]
    X_test[:] = X_test[tf.newaxis, ...]

    model = models.UNet()

    model, evaluation = model.fit(X_train, X_test, Y_train, Y_test,
                                  steps_per_epoch=STEPS_PER_EPOCH,
                                  validation_steps=VALIDATION_STEPS,
                                  epochs=EPOCHS)

    model.save(os.path.join(MODEL_DIR, 'first_try.h5'))
    pred = model.predict(X_train[0][tf.newaxis, ...])
    mask = img_utils.get_mask_as_img(img_utils.probs_to_mask(pred))
    img_utils.save_image(mask, 'result.png')
