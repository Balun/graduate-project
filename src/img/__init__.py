import os

import cv2 as cv
import numpy as np
from PIL import Image

OBSTACLES = 0
WATER = 1
SKY = 2


def probs_to_mask(probs):
    """

    :param probs:
    :return:
    """
    return np.argmax(probs, axis=-1)


def load_image(img_path, grey=False, normalize=False, float=True):
    """

    :param img_path:
    :param grey:
    :param normalize:
    :return:
    """
    img_type = np.float16 if float else np.uint8

    img = cv.imread(os.path.abspath(img_path), 0 if grey else -1).astype(
        img_type)
    return img / 255.0 if normalize else img


def load_images(img_dir, grey=False, normalize=False, float=True):
    """

    :param img_dir:
    :param grey:
    :param normalize:
    :return:
    """

    return np.array(
        [load_image(os.path.join(img_dir, img), grey, normalize, float) for
         img in
         os.listdir(img_dir)])


def show_image(img, normalized=False):
    """

    :param img:
    :param normalized:
    :return:
    """
    if normalized:
        img = img * 255.0

    cv.imshow('image', np.array(img))
    cv.waitKey(0)
    cv.destroyAllWindows()


def save_image(img, file_path):
    """

    :param img:
    :param file_path:
    :return:
    """
    cv.imwrite(file_path, img)


def get_mask_as_img(mask, normalize=False):
    """

    :param mask:
    :param normalize:
    :return:
    """
    img = create_blank_image(mask.shape[1], mask.shape[0])
    pixels = img.load()

    for i in range(img.width):
        for j in range(img.height):
            pixels[i, j] = get_mask_color(mask[j, i])

    if normalize:
        return np.array(img) / 255.0

    return np.array(img)


def show_mask(mask, normalized=False):
    """

    :param mask:
    :param normalized:
    :return:
    """
    show_image(get_mask_as_img(mask), normalized)


def combine_mask_and_img(img, mask_img, alpha=0.7, beta=0.3):
    """

    :param img:
    :param mask_img:
    :param alpha:
    :param beta:
    :return:
    """
    return cv.addWeighted(img, 0.7, mask_img, 0.3, 0)


def create_blank_image(width, height, color='white'):
    """

    :param width:
    :param height:
    :param color:
    :return:
    """
    image = Image.new("RGB", (width, height), color)
    return image


def get_pixel(image, i, j):
    # Inside image bounds?
    width, height = image.size
    if i > width or j > height:
        return None

    # Get Pixel
    pixel = image.getpixel((i, j))
    return pixel


def get_mask_color(code):
    """

    :param code:
    :return:
    """
    if code == OBSTACLES:
        return (255, 0, 0)
    elif code == SKY:
        return (0, 255, 0)
    elif code == WATER:
        return (0, 0, 255)
    else:
        raise ValueError("Illegal color code")


def normalize(img):
    """

    :param img:
    :return:
    """
    return img / 255.
