"""
"""

import tensorflow as tf
from tensorflow import keras


def save_model(model, file_path):
    """

    :param model:
    :param file_path:
    :return:
    """
    model.save(file_path)


def load_model(file_path):
    """

    :param file_path:
    :return:
    """
    return tf.keras.models.load_model(file_path)
