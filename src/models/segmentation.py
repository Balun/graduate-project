"""
"""
import tensorflow as tf

from tensorflow_examples.models.pix2pix import pix2pix


class UNet:
    """
    """

    def __init__(self, input_shape=(384, 512, 3), output_channels=3):
        """

        :param input_shape:
        :param output_channels:
        """
        self.input_shape = input_shape
        self.output_channels = output_channels

        base_model = tf.keras.applications.MobileNetV2(
            input_shape=input_shape, include_top=False)

        # Use the activations of these layers
        layer_names = [
            'block_1_expand_relu',  # 64x64
            'block_3_expand_relu',  # 32x32
            'block_6_expand_relu',  # 16x16
            'block_13_expand_relu',  # 8x8
            'block_16_project',  # 4x4
        ]
        layers = [base_model.get_layer(name).output for name in layer_names]

        # Create the feature extraction model
        down_stack = tf.keras.Model(inputs=base_model.input, outputs=layers)

        down_stack.trainable = False

        up_stack = [
            pix2pix.upsample(512, 3),  # 4x4 -> 8x8
            pix2pix.upsample(256, 3),  # 8x8 -> 16x16
            pix2pix.upsample(128, 3),  # 16x16 -> 32x32
            pix2pix.upsample(64, 3),  # 32x32 -> 64x64
        ]

        # This is the last layer of the model
        last = tf.keras.layers.Conv2DTranspose(
            output_channels, 3, strides=2,
            padding='same', activation='softmax')  # 64x64 -> 128x128

        inputs = tf.keras.layers.Input(shape=input_shape)
        x = inputs

        # Downsampling through the model
        skips = down_stack(x)
        x = skips[-1]
        skips = reversed(skips[:-1])

        # Upsampling and establishing the skip connections
        for up, skip in zip(up_stack, skips):
            x = up(x)
            concat = tf.keras.layers.Concatenate()
            x = concat([x, skip])

        x = last(x)

        self.model = tf.keras.Model(inputs=inputs, outputs=x)

        self.model.compile(optimizer='adam',
                           loss='sparse_categorical_crossentropy',
                           metrics=['accuracy'])

    def fit(self, X_train, X_test, Y_train, Y_test, steps_per_epoch,
            validation_steps, epochs=20):
        self.model.fit(X_train, Y_train, epochs=epochs,
                       steps_per_epoch=steps_per_epoch,
                       validation_steps=validation_steps,
                       validation_data=(X_test, Y_test))

        return self.model, self.model.evaluate(X_test, Y_test, verbose=0)

    def predict(self, x):
        """

        :param x:
        :return:
        """
        return self.model.predict(x)

    def plot_model(self, show_shapes=True):
        """

        :param show_shapes:
        :return:
        """
        tf.keras.utils.plot_model(self.model, show_shapes=True)
