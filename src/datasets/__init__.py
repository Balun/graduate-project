"""
"""

import math


def train_test_split(X, Y, train_ratio=0.8):
    """

    :param train_ratio:
    :return:
    """
    index = math.floor(train_ratio * len(X))

    return X[:index], X[index:], Y[:index], Y[index:]
