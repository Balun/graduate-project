"""
"""
import os

import numpy as np

import src.img as img_utils

DEFAULT_MASTR_DIR = 'res/data/MaSTr1325'
DEFAULT_MODD2_DIR = 'res/data/MODD2'


class MaSTR1325:
    """
    """

    def __init__(self, shape=(1325, 512, 384), path=DEFAULT_MASTR_DIR):
        """

        :param shape:
        :param path:
        """
        self.path = os.path.abspath(path)
        self.img_dir = os.path.join(self.path, 'images')
        self.annot_dir = os.path.join(self.path, 'annotations')

        self.size = shape[0]
        self.img_dim = (shape[1], shape[2])

        self._img_list = [os.path.join(self.img_dir, name) for name in
                          sorted(os.listdir(self.img_dir))]
        self._annot_list = [os.path.join(self.annot_dir, name) for name in
                            sorted(os.listdir(self.annot_dir))]

    def get_data(self):
        """

        :return:
        """
        return self.get_images(), self.get_annotations()

    def get_images(self):
        """

        :return:
        """
        return np.array([img_utils.load_image(img) for img in self._img_list])

    def get_annotations(self):
        """

        :return:
        """
        return np.array([img_utils.load_image(img, float=False) for img in
                         self._annot_list])

    def __iter__(self):
        """

        :return:
        """
        self.i = 0
        return self

    def __next__(self):
        """

        :return:
        """
        if self.i < self.size:
            self.i += 1
            img = img_utils.load_image(self._img_list[self.i - 1])
            annot = img_utils.load_image(self._annot_list[self.i - 1])
            return img, annot
        else:
            raise StopIteration

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        img = img_utils.load_image(self._img_list[item])
        annot = img_utils.load_image(self._annot_list[item])
        return img, annot

    def __str__(self):
        """

        :return:
        """
        return self.path
